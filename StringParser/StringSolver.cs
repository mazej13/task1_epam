﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.IO;


namespace StringParser
{
    /// <summary>
    /// converts the string to a coordinate string
    /// </summary>
    public class StringSolver
    {
        /// <summary>
        /// Convert intput string to coord string
        /// </summary>
        /// <param name="str"> string format of "23.8976,12.3218"</param>
        /// <returns> string format of "X: 23,8976 Y: 12,3218" </returns>
        public string GetCoordFromString(string str)
        {
            if (str == string.Empty && str == null) throw new NullReferenceException("input string is null");

            if (RegexString(str))
            {
                char separator = ',';

                string[] numbers = ReplacePointInInputStrings(SplitInputStringBySeparator(str, separator), separator);
                return String.Format("X: {0} Y: {1}", numbers[0].TrimStart('+'), numbers[1].TrimStart('+'));
            }

            throw new Exception("Incorrect format of input string " + str);
        }

        /// <summary>
        /// Convert string from file to coord string
        /// </summary>
        /// <param name="inputFile"> path of input file</param>
        /// <param name="outputFile"> path of output file</param>
        /// <returns>  string format of "X: 23,8976 Y: 12,3218" </returns>
        public void GetCoordFromFile(string inputFile, string outputFile)
        {

            if (string.IsNullOrEmpty(inputFile) || string.IsNullOrEmpty(outputFile)) throw new NullReferenceException("name intput or output file is null");

            try
            {
                string inputData = null;

                using (StreamReader reader = new StreamReader(inputFile))
                {
                    inputData = reader.ReadToEnd().Replace("\n", string.Empty);
                }

                char endOfLine = '\r';
                string[] convertedNumbersString = SplitInputStringBySeparator(inputData, endOfLine);

                using (StreamWriter writer = new StreamWriter(outputFile))
                {
                    for (int i = 0; i < convertedNumbersString.Length; i++)
                    {
                        string str = convertedNumbersString[i];
                        writer.WriteLine(GetCoordFromString(str));
                    }
                }
            }
            catch (IOException)
            {
                throw new Exception("file not found");
            }

        }

        /// <summary>
        /// check for correctness of the input string
        /// </summary>
        /// <param name="str"> </param>
        /// <returns>true if string satisfies of Regex Expressesion, false in another cases</returns>
        private bool RegexString(string str)
        {
            string pattern = @"^(\-|\+)?\d+\.?\d*,(\-|\+)?\d+\.?\d*$";
            Regex rgx = new Regex(pattern);
            Match mth = rgx.Match(str);

            return mth.Success;
        }

        /// <summary>
        /// Split input string by separator
        /// </summary>
        /// <param name="str"></param>
        /// <param name="separator"></param>
        /// <returns> array of string separeted by separator</returns>
        public string[] SplitInputStringBySeparator(string str, char separator)
        {
            return str.Split(new char[]{ separator }, StringSplitOptions.RemoveEmptyEntries);

        }

        /// <summary>
        /// Replace point in input strings
        /// </summary>
        /// <param name="str"></param>
        /// <param name="separator"></param>
        /// <returns>array of string with removed points</returns>
        private string[] ReplacePointInInputStrings(string[] str, char separator)
        {
            for (int i = 0; i < str.Length; i++)
            {
                str[i] = str[i].Replace('.', separator);
            }
            return str;
        }
    }
}
