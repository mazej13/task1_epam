﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StringParser;

namespace NumerSolutionTest
{
    [TestClass]
    public class StringSolverTest
    {
        [TestMethod]
        public void GetCoordFromStringTest()
        {
            string str = "23.8976,12.3218";
            StringSolver solver = new StringSolver();
            string expected = "X: 23,8976 Y: 12,3218";
           string actual = solver.GetCoordFromString(str);
           Assert.AreEqual(expected, actual);

        }

        [TestMethod]
        public void GetCoordFromStringTestOFIntNumber()
        {
            string str = "-3,6";
            StringSolver solver = new StringSolver();
            string expected = "X: -3 Y: 6";
            string actual = solver.GetCoordFromString(str);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [ExpectedException(typeof(System.NullReferenceException))]
        public void GetCoordFromFileTest()
        {
            StringSolver solver = new StringSolver();
            string inputFile = null;
            string outputFile = null;
            solver.GetCoordFromFile(inputFile, outputFile);
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentNullException))]
        public void GetCoordFromStringTestOfNull()
        {
            StringSolver solver = new StringSolver();
            string str = null;
            solver.GetCoordFromString(str);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Exception))]
        public void GetCoordFromStringTestOfRegex()
        {
            StringSolver solver = new StringSolver();
            string str = "hello";
            solver.GetCoordFromString(str);
        }


    }
}
