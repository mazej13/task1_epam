﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StringParser;
using System.Diagnostics;

namespace NumberSulutionConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("From console press \t1");
            Console.WriteLine("From file press \t2");
            Console.WriteLine("To end write END");

            string answer = Console.ReadLine();
            string paramsToEndInput = "END";
            StringSolver parser = new StringSolver();
            string str = string.Empty;
            List<string> numbersString = new List<string>();
            
            switch (answer)
            {
                case "1":
                    {
                        try
                        {
                            Console.WriteLine("Enter your string");
                            do
                            {
                                
                                str = Console.ReadLine();
                                numbersString.Add(str);

                            } while (str != paramsToEndInput);

                            for (int i = 0; i < numbersString.Count-1; i++)
                                Console.WriteLine(parser.GetCoordFromString(numbersString[i]));

                            break;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message); 
                            numbersString.Clear();
                            goto case "1"; 
                        }
                    }
                case "2":
                    {
                        try
                        {
                            Console.WriteLine("Enter path of input file");
                            string inputFile = Console.ReadLine();
                            Console.WriteLine("Enter path of output file");
                            string outputFile = Console.ReadLine();
                            parser.GetCoordFromFile(inputFile, outputFile);
                            Console.WriteLine("check your output file");
                            Process.Start("Notepad.exe", outputFile);
                            break;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            goto case "2";
                        }
                    }
                default:
                    {
                        Console.WriteLine("Incorrect input");
                        break;
                    }
            }

            Console.ReadKey();
        }
    }
}
