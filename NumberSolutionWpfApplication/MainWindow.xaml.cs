﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StringParser;

namespace NumberSolutionWpfApplication
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
         private StringSolver parser = new StringSolver();
        //private List<string> numbersString = new List<string>();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            textBox2.Text = string.Empty;
            try
            {
                string str = new TextRange(richTextBox1.Document.ContentStart, richTextBox1.Document.ContentEnd).Text.Replace("\n", string.Empty);
                char separator = '\r';
                string[] convertedNumbersString = parser.SplitInputStringBySeparator(str, separator);

                for (int i = 0; i < convertedNumbersString.Length; i++)
                {
                    textBox2.Text += parser.GetCoordFromString(convertedNumbersString[i]) + "\r\n";
                }
                textBox2.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Input error string", MessageBoxButton.OK, MessageBoxImage.Error);
                textBox2.IsEnabled = false;
            }
        }
    }
}
